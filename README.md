# Docker Compose setup for the Ozwillo Project

> This is intended just as a development setup. Don't push any production specific configuration or code, specially secrets.

Based on the main [Docker Compose setup](https://github.com/okfn/docker-ckan) for CKAN.

Git clone your extension in `src/` folder, build and start

```
cd src
git clone https://github.com/ozwillo/ckanext-ozwillo-theme
docker-compose -f docker-compose.dev.yml build
docker-compose -f docker-compose.dev.yml up
```
